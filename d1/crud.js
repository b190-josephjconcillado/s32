// importing the "http" module
const http = require("http");
const port = 4000;
let directory = [
    {
    "name":"Brandon",
    "email":"brandon@mail.com"
    },
    {
    "name":"Jobert",
    "email":"jobert@mail.com"
    }
];
const server = http.createServer((request,response) => {
    if(request.url === "/users" && request.method === "GET"){
        response.writeHead(200,{"Content-Type":"application/json"});
        response.write(JSON.stringify(directory));
        response.end();
    };
    if(request.url === "/users" && request.method === "POST"){
        let requestBody = "";
        /**
         * stream is a sequence of data.
         * data is received from the client and is processed in the data stream.
         * the information provided from the request object enters a sequence called "data" the code below will be triggered
         * 
         *  -"data" step - this reads the data stream and processes it as the request body
         */
        request.on("data", function(data){
            requestBody += data;
        });
        request.on("end",function(){
            console.log(typeof requestBody);
            requestBody = JSON.parse(requestBody);
            let newUser = {
                "name":requestBody.name,
                "email":requestBody.email
            };
            directory.push(newUser);
            console.log(directory);

            response.writeHead(200,{"Content-Type":"application/json"});
            response.write(JSON.stringify(newUser));
            response.end();
        });

    };

});
server.listen(port);
console.log(`Server now running at port: ${port}`);

